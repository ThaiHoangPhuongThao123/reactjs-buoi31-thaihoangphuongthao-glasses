import React, { Component } from 'react'
import { glassArr } from './data'
import ListGlasses from './ListGlasses'

export default class ExGlasses extends Component {
state = {
    glassArr: glassArr,
    urlGlass: glassArr[0].url,
    detailGlass : glassArr[0],
}
hnadleChangeGlasses = (glass) => { 
  let cloneGlassArr = [...glassArr];
  let index = cloneGlassArr.findIndex((item) => { 
    return glass.id == item.id
   })
   if (index != -1) {
    this.setState({
    urlGlass: cloneGlassArr[index].url,
  })
   }
  
 }
 hnadleChangDetail = (glass) => { 
  let cloneGlass = [...this.state.glassArr];
  let index = cloneGlass.findIndex((item) => { 
    return glass.id== item.id;
   })
   if (index !=-1) {
    this.setState({
      detailGlass: cloneGlass[index],
    })
   }
  }
  render() {
    return (
      <div >
        
        <ListGlasses listGlass = {this.state.glassArr}
        urlGlass = {this.state.urlGlass}
       hnadleChangeGlasses = {this.hnadleChangeGlasses} 
       hnadleChangDetail = {this.hnadleChangDetail}
       detailGlass = {this.state.detailGlass}
       />
      </div>
    )
  }
}
