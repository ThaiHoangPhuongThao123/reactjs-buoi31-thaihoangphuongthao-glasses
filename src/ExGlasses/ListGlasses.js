import React, { Component } from "react";
import ItemGlasses from "./ItemGlasses";
import DetailGlasses from "./DetailGlasses";

export default class extends Component {
  render() {
    let { listGlass, hnadleChangeGlasses,detailGlass, hnadleChangDetail} = this.props;
    let i = 0;
    return (
      <div className="container">
        <div className="store p-5">
          <h1 className="text-center">EX GLASSEE</h1>
          <div className="row">
            <div className="col-5 mx-3">
              <h4>Before</h4>
              <img
                className="store-img "
                src="./glassesImage/model.jpg"
                alt=""
              />
            </div>
            <div className="col-5 mx-3">
              <h4>After</h4>
              <img
                className="store-img store-img-glass"
                src="./glassesImage/model.jpg"
                alt=""
              />
              <ItemGlasses urlGlass = {this.props.urlGlass}/>
              <DetailGlasses detailGlass = {this.props.detailGlass}/>
            </div>
          </div>
          <div className="row border border-primary my-3">
            {listGlass.map((item) => {
                 
              i += 1;
              let url = `./glassesImage/g${i}.jpg`;
              return (
                <div>
                    
                    <div className="col-2 m-2">
                  <button
                    className="btn btn-info"
                    onClick={() => {
                      hnadleChangeGlasses(item); hnadleChangDetail(item);
                    }}
                  >
                    <img
                      src={url}
                      alt="button"
                      style={{ width: 100, height: 80 }}
                    />
                  </button>
                </div>
                </div>
                
              );
            })}
          </div>
        </div>
      </div>
    );
  }
}
