import React, { Component } from "react";

export default class DetailGlasses extends Component {
  render() {
    let {detailGlass} = this.props
    return (
      <div>
        <div className="card text-left ">
          <div className="card-body text-success">
            <h4 className="card-title font-weight-bold">{detailGlass.name}</h4>
            <p className="card-text"><strong className="mr-2">Price:</strong>{detailGlass.price} </p>
            <p className="card-text"> <strong className="mr-2">Desc:</strong>{detailGlass.desc} </p>
          </div>
        </div>
      </div>
    );
  }
}
