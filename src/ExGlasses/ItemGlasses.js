import React, { Component } from 'react'

export default class ItemGlasses extends Component {
  render() {
    let {urlGlass} = this.props
    return (
      <div>
        <img className='store-glasses' src= {urlGlass} alt="" />
      </div>
    )
  }
}
